<!--
SPDX-FileCopyrightText: 2021 Martin Häuer <martin.haeuer@ose-germany.de>
SPDX-FileCopyrightText: 2021 Robin Vobruba <hoijui.quaero@gmail.com>

SPDX-License-Identifier: CC0-1.0
-->

# OSH-Guideline | Legal Issues

[![License: CC BY-SA 4.0](https://img.shields.io/badge/License-CC%20BY--SA%204.0-blue.svg)](https://creativecommons.org/licenses/by-sa/4.0/)
[![REUSE status](https://api.reuse.software/badge/gitlab.com/OSEGermany/osh-guideline-legal-issues)](https://api.reuse.software/info/gitlab.com/OSEGermany/osh-guideline-legal-issues)
[![made-with-Markdown](https://img.shields.io/badge/Made%20with-Markdown-1f425f.svg)](http://commonmark.org)

## Overview

**What is the goal of this document?**\
We aim to give an overview over significance of legal aspects to OSH projects.

**Who do we address with this guideline chapter?**\
We aim to address both, legal practitioners as well as laypeople.
While content must be legally accurate,
it shall be understandable for interested individuals with no legal background.
However, a fundamental understanding always helps of course.

**What is covered and what not?**\
We mainly focus on IP law (specifically licensing and patenting)
and liability for defective products.\
**KEEP IN MIND** that his document does not offer legal advice,
it just provides general information with regard to legal issues
associated with open source hardware.
Hence we'll never say: "Do that and you will be safe",
but rather: "don't do that, because there is risk".

**General note:**\
Please keep in mind that this document is a draft only and subject to constant change.
In particular, it may contain inaccuracies or defects not yet remedied.

If you notice any inaccuracy or mistake, please feel free to get in
touch with us and contribute!\
The easiest would be to [drop us an issue](https://gitlab.com/OSEGermany/osh-guideline-legal-issues/-/issues).

We are also working on educational material
to make the knowledge condensed in this guideline accessible for even more people.\
If that's interesting to you,
[this slide show](https://pad2.opensourceecology.de/p/H13RyulYw#/) gives an example.
The open educational resources are currently developed here:
<https://gitlab.com/osh-academy/osh-basics>

Looking forward to e-meet you :)

## Get the Guideline

View/download the current version (of the `main` branch) as

||||
|---|---|---|
|[PDF]|[HTML]|[Markdown][MD]|

These automated exports are made possible by GitLabs awesome CI and [MoVeDo].

There's also a tl;dr available by the EU-H2020-funded project OPEN!NEXT [here](https://github.com/OPEN-NEXT/tldr-ipr).

## Contributing

We don't have a proper contribution guide yet.
However, this section holds some notes that may be important either way:

- Bibliography uses [better CSL JSON](https://github.com/citation-style-language/schema#csl-json-schema)
  (intentionally not bibtex)
  - that's the `bibliography-csl.json` file in the repo
    → it contains all references made in the document
  - to cite something in the document, just use the citation key from the JSON
    (which should work with your literature management software of choice, e.g. Zotero)
  - see [this guide](https://docs.zettlr.com/en/academic/citations/)
    for details (works for Markdown in general, not just Zettlr)
- in case you want to:
  run the following command to render the `MD` file locally using pandoc:

  ```sh
  pandoc -o Guideline.md Guideline.pdf --citeproc
  ```

  for this you will need

  - [pandoc](https://pandoc.org/installing.html#linux)
  - [citeproc](https://github.com/jgm/pandoc-citeproc)
  - a computer with a reasonable terminal
    (e.g. your Linux distribution of choice)

[PDF]: https://osegermany.gitlab.io/osh-guideline-legal-issues/Guideline.pdf
[HTML]: https://osegermany.gitlab.io/osh-guideline-legal-issues/Guideline.html
[MD]: https://osegermany.gitlab.io/osh-guideline-legal-issues/Guideline.md
[MoVeDo]: https://github.com/movedo/MoVeDo/
