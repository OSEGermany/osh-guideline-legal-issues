<!--
SPDX-FileCopyrightText: 2021 Martin Häuer <martin.haeuer@ose-germany.de>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

---
title: OSH Guideline – Legal Issues
subtitle: |
          | **IP Law & Liability**
          | *version:* [${PROJECT_VERSION}](${PROJECT_VERSION_URL})
version: "${PROJECT_VERSION}"
date: "${PROJECT_VERSION_DATE}"
lang: en-US
charset: UTF-8
license: CC-BY-SA-4.0
subject: Guideline
category: Guideline
linestretch: 1.2
fontsize: 11pt
papersize: a4
geometry: "top=2cm,bottom=2cm,left=3cm,right=3cm"
bibliography: ["bibliography-csl.json"]
csl: "ieee-with-url.csl"
description: >
  The following guideline is aimed at providing a private-law understanding
  of how legal issues can affect development, distribution and operation
  of open source hardware (OSH).
  This Guideline provides a general overview
  over Intellectual Property Rights (IP Rights)
  and their interaction with OSH projects,
  the licensing of OSH and civil liability issues regarding OSH.
...

# Legal

## Authors and contributors

- Fabian Flüchter\
  Bucerius Law School, Hamburg - phd student

- Felix Tann\
  Bucerius Law School, Hamburg - research assistant

- Martin Häuer\
  Open Source Ecology Germany e.V. (non-profit)

- Corinna Schäfer\
  CADUS e.V. (non-profit)

- Michael Weinberg\
  Open Source Hardware Association

- Emilio Velis\
  Appropedia

## Licensing

This work is licensed under
Creative Commons Attribution ShareAlike International Version 4.0.
Find the legal code under <https://creativecommons.org/licenses/by-sa/4.0/legalcode>.
Wherever external content has been implemented (e.g. in the form of graphics),
this has been marked out and licensing terms have been respected.

## Disclaimer

Following lawyer's best practice, we start this document with a disclaimer.
Content in this document will never replace a legal advice.
Not a single phrase in this document is meant as such.
We cannot do so since legal cases are often complex and most likely unique,
so we will never tell you: "Do this and you're safe".
What we _can_ do (and aim for by writting this document) is sharing our knowledge
and making it accessible for others,
supporting them to better understand the legal field they are moving in,
e.g. by focussing on practical cases,
using a more intuitive language and the formulation of rules of thumb.\
We hope you enjoy it.

# Overview

**What is the goal of this document?**\
We aim to give an overview over significance of legal aspects to OSH projects.

**Who do we address with this guideline chapter?**\
We aim to address both, legal practitioners as well as laypeople.
While content must be legally accurate,
it shall be understandable for interested individuals with no legal background.
However, a fundamental unterstanding always helps of course.

**What is covered and what not?**\
We mainly focus on IP law (specifically licensing and patenting)
and liability for defective products.\
As described in the [disclaimer](#disclaimer),
this document does not offer legal advice,
it just provides general information with regard to legal issues
associated with open source hardware.
Hence we'll never say:
"Do that and you will be safe", but rather: "don't do that, because there is risk".

**General note:**\
Please keep in mind that this document is a draft only and subject to constant change.
In particular, it may contain inaccuracies or defects not yet remedied.

If you notice any inaccuracy or mistake,
please feel free to get in touch with us and contribute!\
The easiest way would be to drop us an issue ([link](https://gitlab.com/OSEGermany/osh-guideline-legal-issues/-/issues)).

We are also working on educational material
to make the knowledge condensed in this guideline accessible for even more people.\
If that's interesting to you,
this slide show ([link](https://pad2.opensourceecology.de/p/H13RyulYw#/))
gives an example.
The open educational resources are currently developed here:
<https://gitlab.com/osh-academy/osh-basics>

Looking forward to e-meet you :)

# Introduction & Background

## What this Guideline is about

The following guideline is aimed at providing a private-law understanding
of how legal issues can affect development, distribution and operation
of open source hardware (OSH).
This Guideline provides a general overview
over Intellectual Property Rights (IP Rights)
and their interaction with OSH projects,
the licensing of OSH and civil liability issues regarding OSH.

**Important Note:**
There are certainly many other legal topics with regard to OSH
that are worth exploring (like environmental law, for instance).
This guideline does just give a basic overview
over what we feel are some of the most important aspects.\
However, the Guideline itself is open source,
so if you are unsatisfied or notice any errors,
feel free to contribute!

## Legal Components of OSH Projects

In order to better understand how the law can interact with OSH,
it is necessary to identify the components of an OSH project
that are typically affected by legal issues
such as OSH licensing, IP Rights protection and liability.

### Hardware

"Hardware" means all the functional elements of your product
and constitutes the core of your Project.
Hardware can take many forms, but is always physical
– such as circuit boards, engines, robots, furniture, etc.
"Hardware" can mean single components as well as an assembled product
and prototypes as well as finished products.

Hardware will probably be affected by patents, utility models or Design Rights.

### Software

"Software" means any code or firmware.
This includes the source code as well as the object code.

Software will probably be subject to Copyright protection.

### Documentation

"Documentation" is every piece of information that you provide
to allow other users to rebuild your project.
Technical Documentation constitutes the "source code" of OSH.
This makes your hardware free/open source.
It includes descriptions, schematics, designs, guides, manuals, advice, etc.
Documentation can be provided in written form,
in form of audio recordings,
video recordings or given orally,
e.g. in form of a workshop.

Documentation, like Software, will also likely be protected by Copyright.
However, documentation can be subject to patent law instead
when there's no "creative" part in it.
That applies e.g. to a technical drawing of a screw,
which is just a reflection of the functional concept of the screw.
Obviously, the lines are fuzzy here,
so whether a piece of documentation falls under copyright or patent law
may be decided in the specific case.

### Branding

"Branding" means the product's name and any logos attached to your Product,
used on its packaging or on the Documentation.
In some cases, the design of the Product itself can be considered branding,
as long as consumers understand it as a source-identifier.

The Branding of your OSH project
will probably interact with trademarks and trade dresses.

### Distribution Channels

"Distribution Channels" means any way you choose to pass your OSH on to others.
This includes posting it on the internet,
sending it via eMail or making physical or electronical copies
of the documentation and sending or giving these to others.
With regard to disribution channels,
it can make a difference -
especially with regard to liability -
whether you circulate the OSH within a closed group -
e.g. a working group within your company or association -
or if you publish publicly.

### Use

"Use" means every way you may want to use your OSH.
It is important to note
that it can make a great difference with regard to IP rights or even liability,
whether you plan on using your OSH privately or commercially.
"Private use" means use in a very limited scope,
e.g. you only built the OSH for yourself,
your family or for a couple of close friends for free.
"Commercial use" basically means using OSH in exchange for money.
In particular IP rights often have an exception for private use.
This means that you will not infringe these rights,
if you use e.g. patented OSH only privately.

## Important legal issues concerning OSH

**Protecting my OSH… against what exactly?**

It might seem paradoxical to try to protect OSH
which is supposed to be available for everyone.
However, using the law in the right way
can be important to protect exactly the openness of OSH
and on the other hand fairly attribute liability
to the people who are willing to take responsibility
for the development, distribution and use of OSH.

### Infringement of OSH and Remedies

There might always be the risk that downstream users do not share their contributions,
that OSH is used without proper references or attribution
or that initial inventors are locked out of their own project.
In this regard, there seems to be no difference between OSH and FOSS.

As in case of FOSS, legal rights like Patents or Copyright
can help keep projects open source in the long run
and help ensure everyone gets the credit that is their due.

### Liability for Development, Distribution and Use of OSH

If you are developing OSH and want to make it available for free to benefit others,
you will probably not want to be held liable if something goes wrong
and e.g. people get injured or other IP Rights get infringed.
The threat of liability –
be it for IP infringement or the payment for material or personal damage –
can be a real obstacle to the development of OSH.

This is why we want to explain the general risks
and explore possibilities of how to possibly avoid liability.

**IMPORTANT NOTE:**
The remarks in this Guideline do not constitute legal advice.
They are meant to generally describe legal issues
and things to avoid when dealing with OSH.

## IP Protection for OSH – General Remarks

### What are IP Rights and how are they relevant to OSH?

The term "IP" is short for Intellectual Property.
In general, IP Rights grant the exclusive rights over the protected subject matter.
This means, the owner can prevent others from building,
making, copying, using, distributing, selling or offering for sale
the protected OSH (see e.g. 35 U.S.C. 271 [@35271Infringement]
or Sec. 9 PatG (§§ 139 - 142b) [@Patentgesetz]).

This can be for example Hardware, Software, Documentation or Branding.
In short, IP Rights can enable you to - or prevent you from -
realizing your OSH Project.

IP rights can be distinguished by whether they arise automatically
or have to be registered to grant you exclusivity.
While for example Copyright generally arises automatically,
other IP rights, like patents, must regularly be registered to grant a monopoly.
This is important, because you must consider that registration of such a right
usually costs money as well as time.

### Which Remedies are generally available?

Generally, if someone uses your OSH in a way that _violates_ its open character,
as long as it is protected by IP Rights,
you can stop them from using the OSH (cease and desist),
you can claim damages (e.g. for lawyer's costs),
make them recall their products
and destroy all the infringing goods and material.

With regard to the calculation of damages, in some jurisdictions like the US,
treble damages might be awarded to compensate the right holder.
In other countries, this is not permissible.
Damage calculation is usually based on one of three possibilities:

- hypothetical licence fee,
- actual damage of the right holder,
- taking away the infringer's profits.

### Territorial Scope of IP Rights

One important property of IP Rights
is their usually territorially limited scope of protection.
This means for example
that a patent registered only in Germany is generally only valid there
and only protects from infringements committed in Germany.

This also applies to non-registered IP Rights such as Copyright,
but the difference is that these rights generally arise everywhere at the same time.[^cn-nonIP]
This does however not mean that you can have one global Copyright,
but Copyright which offers protection different from state to state for your OSH.

[^cn-nonIP]: citation needed

### Sources of International Law

For most IP Rights, there are international contracts
that set a certain minimum standard of - and requirements for - protection.
One of the most important contracts for material IP law
is the Agreement on Trade Related Aspects of Intellectual Property Law (TRIPS) [@WTOIntellectualProperty].
Other important agreements include the Berne Convention [@BerneConventionProtection]
and the Paris Convention [@dowie-whybrowParisConventionProtection2013].

## Individual IP Rights relevant to OSH

### Patent

#### What is a patent?

A patent is an exclusive IP right that essentially covers functional inventions
in contrast to aesthetic creations,
which are generally not covered by patents.
It grants a monopoly on building, selling, offering for sale or using
the patented invention (see e.g. Sec. 9 PatG (§§ 139 - 142b)) [@Patentgesetz]).

Patent protection can only be acquired through registration of the patent
with the competent patent office.[^natpato]

To be patentable, an invention generally must fulfill the following requirements:

[^natpato]: Regularly, these will be the national patent offices,
e.g. USPTO (USA) or DPMA (Germany).

##### 1. Patentable subject matter

Patentable subject matter means
that the invention covered by the patent must be eligible for patent protection.
As a general rule of thumb, this is usually the case, when

- the invention is functional, not not purely aesthetic and
- it is not vivid (does not refer to the human body or human genes,
  plant varieties or animal breeds (Sec. 1a [@1aPatG], Sec. 2a PatG [@2aPatG])) and
- it does not violate public policy will not be patented (cf. Sec. 2 PatG [@PatG]). <!-- TODO There seems to be a c&p error here; the sentence does not make sense to me -->

An OSH example of a patentable Project violating public policy
might be the "Liberator" gun.
This gun, first made in 2013,
became known as the first 3D-printable firearm design
made publicly available online.
The prints were downloaded over 100.000 times
before they were removed from public access (cf. [@USGovernmentOrders2013]).

Even if this design was in principle patentable,
it remains open for discussion if the patenting of an untraceable
and undetectable gun would violate public policy.[^nopubguns]

[^nopubguns]: For these reasons, in 2018, a restraining order was issued not to
    make the gun desgins publicly available, see [@JudgeBlocksRelease2018].

##### 2. Novelty

To be patented, an invention must be new.
To be new, an invention must not be part of the state of the art.
The state of the art is basically defined as every information
available to the relevant public at the time of patent application.

With regard to OSH,
the novelty requirement is probably the most important factor
in applying for - or avoiding - patents.

##### 3. Non-obviousness / inventive step

An invention must also not be obvious.
This means that trivial inventions will not be rewarded with a patent.

In short, inventions that would be regarded by a person skilled in the art
as already invented with reference to the state of the art,
are obvious (for a broader overview see [@PatentabilityNonobviousnessRequirement2017]).

Unfortunately, the assessment of obviousness is highly fact-specific,
so there is no general rule.
However, non-obviousness depends on
(I) the scope of the prior art,
(II) the differences between prior art and claimed invention and
(III) the level of ordinary skill in the pertinent art (see [@UnderstandingObviousnessJohn2015]).

Although as of now,
we are not aware of cases regarding the patenting of OSH and non-obviousness,
this could be a considerable obstacle,
preventing OSH developers from applying for a patent.
Since OSH innovation is usually based on the exchange of knowledge
and reference to the Projects of others,
non-obviousness might be difficult to prove in practice.

##### 4. Usefulness

The utility requirement is a low threshold requirement,
and means that the invention has to work (see [@UnderstandingPatentLaw2015]).

The same applies to OSH-Projects.
Non-functioning prototypes, for example,
will therefore most likely not be patentable.

##### 5. Term of a Patent

If granted, a patent generally has a maximum term of 20 years.

#### How are patents relevant to Open Source Hardware?

Patents can be kind of a double-edged sword for OSH projects
and are important to OSH in two ways:
For the patent owner, they can enable development and distribution of OSH.
Makers faced with Patent claims on the other hand,
might be liable for patent infringement.

##### Patents as OSH-Enablers

Patent rights will usually protect the Hardware of a Project,
if the requirements necessary for patent protection are met.
Similar to Copyright, Patent protection allows you
to license your Hardware under patentleft
and keeps it open for others to use, distribute
and further develop or just to tinker with it.
As such, patents can be essential
for making your project open source in the first place.

The protection of Software (as part of a Hardware Project)
is a more complex issue.
Patentability of Software will very likely depend on the country
where patent protection is sought (cf. [@Copyright]).

Some countries refuse patent protection for Software _as such_,
because of the code having no technical character.
On the other hand, they might allow patenting the underlying algorithm,
i.e. the _function_ of the software code.

As a very general rule of thumb,
it is likely that a software-related invention
(e.g. a hardware product which includes firmware)
having a technical character can be patentable (cf. [@Copyright]).
On the other hand, software is usually automatically protected by Copyright
as soon as it is created
and therefore Copyright presents another viable option
for protecting the Software-part of an OSH project.
The downside of this is,
that Copyright does in general only protect the literal code of the respective software
and not the concept behind (cf. [@Copyright]).
For further details, see below under the chapter [Copyright](#copyright)
and for the licensing of Copyright under the chapter [Licensing](#licensing).

##### Patents as a possible Threat to OSH

Liability for patent infringement for:

- Producers: if commercial
- Developers/Contributors: depends on use
- Users: not much risk for private use
- Distributors: probably (offering of means to infringe patent;
  maybe private use ecxeption applicable)

### Utility Model

#### What is a Utility Model?

A Utility Model can be compared to a Patent,
but requirements are generally not as high,
and the scope of protection not as extensive
(see [@UtilityModels],
under "What are the main differences between utility models and patents?").

Similar to a Patent, Utility Models protect technical inventions
through a limited exclusive right.
That means no one but the Utility Model owner is allowed,
during that time, to commercially exploit the Utility Model
(see [@UtilityModels], under "What kind of protection does a utility model offer?").

#### Requirements for a Utility Model

Since no international framework exists for the material law of utility models,
at this point,
the requirements for Utility Models can only be described very broadly.

Generally, to be eligible for a Utility Model,
an invention must, like a Patent, always be novel.
On the other hand, the requirement for an inventive step are generally lower
compared to a patent
(see [@UtilityModels],
under "What are the main differences between utility models and patents?").
The term for a Utility Model is also lower than a Patent term.
Usually, it is between 6 and 15 years [@UtilityModels].

#### How are Utility Models relevant to OSH?

Utility Models can essentially provide similar benefits to your OSH project
as Patent protection does.
Compared to a Patent application, however, the requirements are lower
and registration is often faster and cheaper [@UtilityModels].
For example, according to WIPO, on a local level,
the requirements for an inventive step might be lower than for a patent,
or absent altogether [@UtilityModels].
Essentially, depending on the law of the relevant country,
Utility Models might not need to be as inventive or innovative as patents.
On the other hand, it is likely that protection for a Utility Model
is not as extensive as Patent protection,
e.g. because it is not available in every country
and is likely to differ, since there is no international framework.

In summary, Utility Models can be a real alternative to Patent protection,
since they are also able to protect the functional elements of an OSH invention.
On the other hand, especially on an international level,
it might be difficult to obtain the desired level of protection.

### Copyright

#### What is Copyright?

Copyright is an exclusive IP right that covers creative works.
It protects inter alia from reproduction or publication. <!-- TODO: explain what "inter alia" means -->

It is important to note that copyright protection only extends to creative elements,
and not purely technical features
(but these may be eligible for a patent, [see above](#patent))
Also, copyright only protects concrete expressions and not abstract ideas,
such as procedures, methods of operation or mathematical concepts as such
(see Art. 9 para. 2 Berne Convention [@BerneConventionProtection]).

Another special feature of copyright,
is that for a work to be protected under copyright,
it is not necessary that the work is registered in any form.
Copyright protection arises automatically with the creation of the work itself
(see Art. 5 para. 2 Berne Convention [@BerneConventionProtection]).

Copyright protection lasts as long as the lifetime of the author
and a minimum of fifty years after her death
(Art. 7 para. 1 Berne Convention [@BerneConventionProtection]).

Examples for copyrighted works include literary works, computer programs,
databases, sculpture, architecture as well as advertisements,
maps and technical drawings (see [@Copyright]).

##### Helpful background knowledge

###### International Sources of Law

Internationally, the most important Treaty administering Copyright is the Berne Convention.

The Berne Convention was adopted in 1886,
and deals with the protection of works
and the rights of their authors [@BerneConventionProtectionb].
It has a total of 179 member states [@WIPOLex].

###### Copyright protected works

The Berne Convention rather protects specific types of works
than sets general criteria for copyright protection.
The types of works protected under the Berne Convention
do not specifically mention works of OSH.
Art. 2 para. 1 Berne Convention however specifically mentions "literary works"
in the scientific and artistic domain
as well as "works of drawing, painting, architecture, sculpture, engraving and lithography;
\[\...\] works of applied art; illustrations, maps, plans,
sketches and three-dimensional works relative to \[\...\] science".
The Berne Convention therefore aims at a rather broad scope of protection
and is specifically not limited to artistic works.

The TRIPS Agreement expands on the issue of protected works.
Art. 9 para. 2 TRIPS clarifies for instance
that Copyright protection shall not extend to ideas,
procedures, methods of operation or mathematical concepts as such.
In addition, Art. 10 para. 1 TRIPS provides that the Computer Programs,
whether in source or object code,
shall be protected as literary works under the Berne Convention.

This particular scope of protection means for OSH,
that in principle all relevant parts of an OSH project
(Hardware, Software, Documentation, Branding)
can be eligible for Copyright protection.

###### Rights under Copyright

Under Copyright, the owner has economic as well as moral rights.

Economic rights concern the commercial exploitation of a protected right,
in particular protection from reproductions or publication of the copyrighted work
without the author's consent (see Art. 9 para. 1 [@BerneConventionProtection]).

Moral rights concern the author's personal relationship to her work.
Important moral rights are the claim to authorship
or the possibility to object to any distortion, mutilation or other modification
of the work (see Art. 6 para. 1 [@BerneConventionProtection]).

#### How is Copyright relevant to Open Source Hardware?

Copyright can attach to your OSH Project at several points.

To obtain Copyright protection for the hardware of your Project might be rather difficult.
Purely functional or technically necessary parts of hardware
will probably not be protected by copyright.
Aesthetic or design features, on the other hand,
may constitute protectable subject matter.[^OSHWA-hw-spectrum]
This might also apply to underlying technical documentation
needed for the reproduction of hardware components.
It might generally fall under copyright protection,
if it meets a certain threshold of originality.

[^OSHWA-hw-spectrum]: Visit <https://certification.oshwa.org/process/hardware.html>
and click on the "spectrum" tab to get some well-illustrated examples.

On the other hand, Software in connection with your OSH-Project
is usually subject to Copyright protection (cf. [@OSHWACertificationProcessa]).
This includes any code, firmware or other Software
relating to the functionality of your Project.
It is important to note, however,
that Copyright protection only extends to the specific work,
i.e. the code, not the rest of your
Project.

The Documentation to your Project may also be protectable under Copyright.
As a rule of thumb, the more creativity a Documentation expresses,
the more it is likely to be protected under Copyright (cf. [@OSHWACertificationProcessb]).

This rule of thumb also applies to the Branding of a Project.[^OSHWA-bran-spectrum]
The more creative the Branding is,
the more likely it will be eligible for Copyright protection.
For example, single words as such will most likely not be protectable under Copyright,
while particularly creative logos might be protectable.

[^OSHWA-bran-spectrum]: Visit <https://certification.oshwa.org/process/branding.html>
and click on the "spectrum" tab to get some well-illustrated examples.

The fact that your Branding might simultaneously be protected under Trademark law
does not exclude Copyright protection [@WIPOLex].

In summary, Copyright protection for your Project
will be most likely for design features of the Hardware,
corresponding Software and creative expressions in the Documentation and Branding.

### Trademark

#### What is a Trademark?

Trademarks are signs which serve the function of distinguishing the goods or services
of one enterprise from those of another [@Trademarks].
Trademark rights confer the exclusive right to use a trademark.
That means, the protected sign cannot be reproduced
without the trademark owner's consent.
In addition, others may also be deterred from using a sign
that is confusingly similar to the owner's trademark
(Art. 16 para. 1 TRIPS Agreement [@WTOIntellectualProperty]).

In order to obtain Trademark protection, however,
the sign must be registered with the competent Trademark Office.
The Trademark rights are then limited to the country/association of states
where the Trademark is registered.

As Patents and Copyright, Trademarks are territorial rights.
For example the holder of a US-Trademark cannot bring action for Trademark infringement
of the same sign in Germany based on the US-Trademark.
In order to protect against this case, a German Trademark would be necessary.

#### Sources of international law for Trademarks

The most important international sources for material trademark law are
the Paris Convention for the Protection of Industrial Property
(Art 6 ff. [@dowie-whybrowParisConventionProtection2013])
and the TRIPS agreement (Art. 15 ff. TRIPS Agreement [@WTOIntellectualProperty]).
The international procedural law is mostly governed by the Madrid Agreement
and related Protocol as well as
the Nice Agreement (classification of goods and services) and
the Vienna Agreement (classification for figurative trademarks).

The international Trademark law guarantees a certain minimum standard
for the subject matter of Trademarks and their protection.
In addition, it provides rules to protect foreigners from discrimination
when they apply for a Trademark in another member state of the agreements.
Furthermore, the procedural rules in particular
allow for the subsequent application for multiple national Trademarks
based on an international application (so-called IR-Trademark) (cf. [@WIPOMadridInternational]).

#### Requirements for Trademark Protection

For a sign to be registrable as a Trademark,
it must be able to distinguish the goods or services of one undertaking
from those of other undertakings.
Apart from that, every sign or combination of signs
can in theory be registered as a Trademark
(Art. 15 para. 1 TRIPS Agreement [@WTOIntellectualProperty]).
This includes words or combinations of words
as well as figurative elements or combinations of words with figurative elements.[^OSHbrands]
Apart from these most common forms of Trademarks,
other forms (3D-shapes, colors, scents, etc.) may also be protectable.
Even the overall appearance of a product
(e.g. bottle shapes, customized boxes which contain the actual product, etc.)
can be protected as a Trademark (so-called Trade Dress),
provided it can serve as a source-identifier.[^tradedressdetails]

[^OSHbrands]: some examples for OSH trademarks:
[Arduino](https://www.arduino.cc/en/trademark),
[Lulzbot](https://www.lulzbot.com/about),
[PRUSA RESEARCH](https://trademarks.justia.com/881/63/prusa-88163904.html),
[MNT REFORM](https://github.com/mntmn/reform#license).

[^tradedressdetails]:
For an overview over the differences between Trade Dress and Design,
see [@DifferencesDesignPatents2016].

Another requirement for Trademark Protection
can be that the Trademark is actually used by the Trademark owner.
In these cases, however, the Trademark cannot be canceled
before a period of three years of uninterrupted non-use
(Art. 19 para. 1 TRIPS Agreement [@WTOIntellectualProperty]).

As a rule of thumb, a Trademark protection is more likely to be registered
and more robust, the less descriptive the sign is for what the Project does.
Generic or very descriptive signs might not be registrable as a Trademark at all.[^OSHWA-bran-spectrum]

#### How are Trademarks relevant to Open Source Hardware?

Trademarks may protect the Branding of a Project.
Examples for forms of Trademarks are the Project name,
a logo or even the three-dimensional shape of the Project.

Trademarks provide the benefit that they are relatively easy and cheap to obtain.
For example, for the registration of one Trademark in one class,
the USPTO requires fees in the amount of \$ 225,- [@TrademarkFeeInformation].
The process of registration on the other hand takes at least 10 months (cf. [@SectionTimeline]).

It is important to note, however,
that Trademark protection only prevents others only from using your branding,
but not from copying your OHS or its functions [@OSHWACertificationProcessc].
Trademark protection may however be useful to protect a certain standard of quality.
An example for this is the OS-circuit board manufacturer Arduino,
who sells pre-assembled circuit boards, but also do-it-yourself kits
and offers knowledge about configuration, coding and use for free.
Arduino also protects its name with a trademark,
possibly to guarantee a certain standard of quality for official products,
which may support their business model.

In general, trademark protection may have the benefit
that consumers can be assured to buy a quality product
when the purchased product is branded with a highly reputed trademark.
It is also possible to register so-called certification marks
that can be used by several companies
and that do not act as a source identifier,
but indicate compliance with predefined standards.
A famous example is the mark "WOOLMARK",
which certifies that the relevant goods are made of 100% wool [@organizationMakingMarkIntroduction2017].

In summary, Trademark protection has the benefit
that it is relatively cheap and easy to obtain.
Although Trademarks do not prevent misuse of the Hard- or Software in your Project,
Trademarks may still be useful to protect the goodwill represented in your Project
or to guarantee a certain standard of quality when others are using your OSH.

### Design Rights

#### What is a Design Right?

Design Rights, also referred to as "Design Patent" or "Industrial Design",
are exclusive IP rights that protect aesthetic or ornamental features of Products
and prevent others from copying them [@IndustrialDesigns].

Design Rights, as Patents and Trademarks,
regularly require registration for legal protection of a Design
(cf. e.g. in Germany [@DPMADesignschutz]).
The term of Design Rights is at least 10 years after registration
(Art. 26 [@WTOIntellectualProperty]).

#### Sources of international law for Design Rights

International material law for Design rights
is essentially governed by the TRIPS agreement in Art. 25 f. TRIPS.

#### The requirements for design protection

Art. 25 TRIPS provides the international minimum standards for Design protection.
In order to be protected, industrial designs must be new and original.

TRIPS also provides some guidance with regard to the interpretation
of the terms "new" and "original".
For instance, TRIPS provides that industrial designs
which do not significantly differ from the prior art
may not be considered new and original.
Similarly, TRIPS allows members to exclude industrial designs from protection,
if the design features are mostly dictated by technical or functional considerations.

In summary, as a general rule of thumb, industrial designs, much like a patent,
should be eligible for protection if they are different from the state of the art
and, much like copyright,
if their features are not defined by technical or functional considerations.

#### How are Design Rights relevant to OSH?

Like a patent, Design Rights grant the owner the exclusive right to copy,
distribute or sell the protected Design
and can therefore be useful in protecting the hardware of an OSH,
especially if it is not eligible for patent protection.
Also, in contrast to Copyright,
Design Rights should also protect the owner from independent creations by others,
i.e. the creation of the same object without knowing or copying the protected OSH
(cf. [@beebeDesignProtection2017]).

On the other hand,
Design Rights may not protect functional aspects of Hardware.
This means that Design Rights do not prevent anyone to copy the functions
of an OSH project and simply change its design features.
In this regard, it is important to consider the relation between the design
and functional features of OSH.
In cases where the design features are tied to OSH's functional features,
(e.g. via best practice solution)
this can theoretically extend Design protection to functional features
to a limited extend.
On the other hand, the closer functional elements are tied to design features,
the less likely OSH is to be eligible for Design protection.

### Trade Secrets

#### What is a Trade Secret?

Trade Secret protection grants the owner a right
to protect against unlawful acquisition of valuable information.
Unlike Patent or Copyright, it is not an exclusive right
and does not protect against the independent discovery of the relevant information.
Trade Secret protection arises automatically,
registration is not necessary.
The term of protection for Trade Secrets is theoretically infinite,
since protection depends mostly on secrecy of the protected information.

#### Sources of international law for Trade Secrets

Material Trade Secret Law is most importantly governed by Art. 39 TRIPS.

#### Requirements for Protection and Infringement

Art. 39 TRIPS provides three requirements for Trade Secret Protection.

The information must:

- be secret in the sense that it is not commonly known,
- have commercial value _because_ it is secret,
- and must have been subject to reasonable steps to keep it secret.

In this regard, according to Art. 39 TRIPS,
secrecy means that the relevant information is,
as a body or in its precise configuration,
not generally known to people that usually deal with this kind of information.
This standard may be comparable to a person skilled in the art.[^54]
Reasonable steps to keep the Trade Secret secret means
that the owner must take some precautions to avoid the information becoming known,
e.g. encryption of files,
restriction of access to rooms where prototypes are stored,
marking documents as "confidential", etc.

[^54]: \[\...\]

Infringement is defined by TRIPS as disclosure or acquisition
without consent of the Trade Secret owner
in a manner contrary to honest commercial practices.
While the term "honest commercial practices" is open for interpretation,
this specifically includes breach of contract,
breach of confidence and inducement to breach
(see Art. 39 TRIPS Agreement [@WTOIntellectualProperty]).
In practice, this will likely include stealing confidential Documentation or Prototypes
or breaching an explicit agreement not to make available any information
to third parties.

#### How are Trade Secrets relevant to OSH?

Intuitively, the practice of keeping Trade Secrets
violates the openness and disclosure of information
promoted by Open Source principles, in particular OSH.
However, Trade Secrets may still be useful
in preparing the roll-out to the public of your OSH-project.

Especially when there is no other way to protect the Hardware of your OSH-project,
Trade Secrets may be a useful way to prepare the application for other IP-rights
such as a Patent or Utility Model
and protect you from being locked-out of your own project
(see ch. [Keeping your Freedom to Operate](#keeping-your-freedom-to-operate)).
Once you release your project to the public,
Trade Secret protection automatically terminates
and no one, especially not third parties,
may enforce Trade Secret protection,
since the relevant information is no longer secret.

In summary, while Trade Secret protection may seem contraintuitive at first,
it can be an important component in the protection strategy for any OSH-project.

### Summary

From the above it becomes clear
that sufficient protection for OSH,
especially for hardware, is not easy.

Generally, the only IP- or similar rights that arise without registration
are Copyright and Trade Secret Rights.
While, as a rule of thumb,
Copyright can be suitable to protect a Project's Software,
Documentation and maybe Branding,
it will rarely grant suitable protection for Hardware
and excludes protection for functional elements altogether.
Trade Secrets on the other hand
can help prepare the registration of rights like Patents or Designs,
but are probably ill-suited to protect OSH during distribution,
because they require secrecy of the information, not openness.

Nevertheless, there are licensing an other strategies
to help the distribution of OSH and to maintain openness.
These strategies will be discussed in the following
based on the above described rights.

In a nutshell, the relationship between IP-rights available to OSH
and its relation to different parts of an OSH-Project
are summarized in the following figure
\ref{fig:ip-chart} ("**R**" means "registered right"):

![](illustrations/IP-overview-chart.png){#fig:ip-chart width=100%}

# Practice

## IP Strategies for important OSH use cases

To find the "right" way to deal with IP issues
in the OSH context can be challenging,
because there are so many rights
each with different requirements and effects.
That is why, just as a general rule of thumb,
below we summarized some very general considerations
that might be of help.

**Note:**
Please observe that these considerations are only academic in nature
and dot not constitute or substitute legal advice.
In particular, these considerations are subject to the particularities
of each individual case.

### Using OSH privately

If you simply wish to use OSH for private purposes,
i.e. yourself, your family, etc.,
you may not have to observe any IP-related restrictions,
as most IP Rights in most countries have an exception for private use.

Private use is usually defined in contrast to commercial use.
In addition, it is usually limited to your private area, i.e. family and close friends.
Commercial use essentially means to make money with something.
As a rule of thumb,
you should therefore probably be able to use IP-protected material
if you are not making money with
and if you are using it exclusively in the private area.
However, this does not mean that you can make unlimited use of OSH
as long as you are not making money with it.
National IP laws will probably have some restrictions in place
on how much you can use a protected Product.

Background:

For example, § 53 para. 1 German Copyright Act stipulates:

> Individual reproductions of a work by a natural person
> for private use on any medium whatsoever are permitted,
> provided that they are not used either directly or indirectly
> for profit-making purposes, provided that
> no obviously unlawfully produced or publicly accessible template
> is used for reproduction.

What exactly the extend of "individual reproductions" is,
has not yet been conclusively and probably depends on the individual case.
In one decision, the German Federal Court said
that it should not be more than seven copies of a work
(see BGH, decision I ZR 111/76 of 14. April 1978 - Vervielfältigungsstücke).

In US-Copyright Law, private use of copyrighted material
is part of so-called Fair Dealing,
if the following requirements are met:

**[TODO]**

<!-- investigate requirements -->

### Publishing OSH

If your only goal is to publish OSH
and you don't really care what happens to it afterwards,
you will probably not need any registered rights like patents for that.
In theory, it could be enough to use the mechanism of Defensive Publishing,
i.e. making the functional part of your OSH project
part of the public domain (mechanisms, concepts etc.).
In that way, it will probably not be appropriated by someone else.
For more details on Defensive Publishing,
see the chapter [Defensive Publishing](#defensive-publishing).

Please note that the scope of Defensive Publishing
is probably limited to the _current state_ of the functional part
of the OSH you publish
and will probably not extend to any amendments or further developments of it.

Furthermore, the technical documentation will most likely require
to be published under a free/open license
in order to enable others to use it.

### Contributing to OSH

If you have an already existing OSH Project to which you want to contribute,
you will probably have to adhere to the use or licensing conditions
of that Project to be able to contribute in the first place.

In order to reduce the risk of liability,
it might also be beneficial to publish any contributions
through an entity with limited liability,
like a LLC or maybe a charitable organisation.
For more details on liability,
in particular for damages,
see the chapter [Liability risks in OSH](#liability-risks-in-osh).

### OSH as a businesses core asset

If you want to start a business with the OSH as part of your core assets,
it might be beneficial to initially develop the Project
under protection of trade secrets
and then subsequently apply for a patent protection
to better be able to control the free flow of OSH after release to the public.
Additionally, trademark protection for the names of goods and services
in connection with the OSH might be worth considering.

For more details on how trade secret protection and patents can help
with licensing and avoiding lock-out
(see ch. [Keeping your Freedom to Operate](#keeping-your-freedom-to-operate)).

For more details on why trademarks can be helpful in the OSH context,
see the chapter [Trademark](#trademark).

### Joining OSH and other components

Building a complex Product that in part contains OSH
and a part or parts that you want to keep proprietary
could be very difficult from a legal point of view.
In the end you should be prepared for the possibility
that the whole project becomes OSH
and you have to adhere to the use or licensing conditions
of the OSH you integrated into the Project.

Whether or not a Project with OSH and other parts
can be considered OSH as a whole
probably depends on how much of it was OSH in the first place
in terms of quantity of OSH components
as well as how the components work together.

## Licensing

**How to make your Project Open Source?**[^OSHWAcert]

[^OSHWAcert]:
OSHWA offers detailed information with regard to OSH certification,
which is also used in this chapter;
see [@OSHWACertificationProcessd].

### Introduction: Why is licensing important for OSH?

Proper licensing is critical for an OSH Project, because without it,
the licensed hardware would not remain open for downstream users.

Many parts of OSH are, unlike FOSS, not automatically protected by exclusive rights.
Without a contract in form of a license,
your project and any derivatives might be appropriated by downstream users,
which violates the fourth and fifth principle of OSH (cf. [@DefinitionEnglish]).
In the worst case, you might be locked out from using your own idea
(see ch. [Keeping your Freedom to Operate](#keeping-your-freedom-to-operate)).

This chapter of the guideline addresses the issues regarding
how to license all parts of your project to make them open source.
First, we are going to identify the parts of a project that must be licensed.
Then, we explain which intellectual property (IP) rights are concerned
when licensing OSH and how they may attach to components of a project.
Then we explain how OSH-licenses work in general
and discuss the advantages and drawbacks of customary licenses.

In case there are exclusive rights in play,
we also give some examples for other mechanisms to make them openly available.

### What parts of a project must be published under a free/open License?

**[TODO]**

<!-- probably everything that falls under copyright such as […] + anything patent-related to automatically use the defensive publishing mechanism -->

### How do OSH-licenses operate generally?

OSH licenses can generally be classified into different categories.
The most important ones probably being “Permissive Licenses” and “IPleft Licenses”
(see [OSWHA - "What license should I use?"](
https://www.oshwa.org/faq/#what-license-to-use under>)).

IPleft Licenses (most often called “Copyleft” or “share-alike”;
since OSH usually consists of more than one IP right and not only Copyright,
we  refer to these OSH licenses as “IPleft” licenses)
require the licensee using or modifying the licensed material
to make derivative material available under the same conditions as originally acquired.
This means that if you download a design and change it,
you are required to publish it under the same conditions as the original design.
This way, the design and any improvements remain open for anyone else’s use.
Usually, this includes the condition that the licensed material and any derivatives
are made available free of charge,
otherwise it would not be free to use for subsequent Licensees.
This type of license is therefore probably most useful
if you want your material to be available also for downstream users.
IPleft Licenses include
GPL 3.0,
CC-BY-SA 4.0,
CC-SA 4.0,
CERN OHL 1.2,
2.0-s,
2.0-w
and TAPR.

Permissive Licenses on the other hand
do not require you to make your modifications available.
Proprietary derivatives are possible.
For instance, as Licensee, you could sell the licensed material or any modifications
or keep it to yourself.
The license can, however, sometimes require you to name the original author
of the licensed material.
As Licensor, this type of license is probably best
if you just want to publish your material and want anyone to use it without any restrictions.
Permissive OSH licenses include CC-0, Solderpad and CERN OHL 2.0-p.

#### The anatomy of OSH-licenses

The following section describes the essential structure of OHS licenses.
They usually consist of a Preamble, Definitions, Scope/Appicability,
Rights and Obligations, Disclaimer and Liability and Termination.

##### Preamble

The Preamble says what the general purpose of a license is.
In case of OSH-licenses,
the purpose usually is to promote free access and distribution of ideas and OSH Projects.
While a Preamble may be helpful to understand a license,
it is not strictly necessary for the license to be legally binding.
The Apache-license 2.0 for instance, does not have a Preamble.

##### Definitions

This is a license’s own dictionary.
Terms that are important to the license most often have a special meaning.
To avoid repetitions and make a license easier to understand,
this meaning is defined in the “Definitions” section of a license.
Definitions are usually written with capital initial letters throughout the license.

The well-known Creative Commons License BY SA 4.0 defines “Licensed Material”
as “artistic or literary work, database, or other material
to which the Licensor applied this Public License”.

Sometimes, Definitions can be more difficult to grasp,
especially when they refer to each other or overlap.
An example is the Cern OHL 2.0 – Strongly Reciprocal License.
There, the term “Source” –
which is similar to the “Licensed Material” in the CC BY-SA 4.0
is defined as <!-- TODO a "which" seems to be missing at the start of this line -->
“information such as design materials or digital code
which can be applied to Make or test a Product or to prepare a Product for use,
Conveyance or sale, regardless of its medium or how it is expressed.”
In order to fully understand the meaning of “Source”,
it is therefore necessary to also understand the definitions of “Make”,
“Product” and “Conveyance”.

##### Scope/Applicability

This section describes the place and time to which the license applies.
Usually, OSH licenses apply worldwide and indefinitely.

Sometimes, licenses also mention the type of use to which a license applies.
For instance, the Cern 2.0 – s license provides that it applies to the
“use, copying, modifiying and Conveying of Covered Source
and Products and the Making of Products”.
In its essence, this means that the lincense will apply to everything you want
to do with material provided to you under the license.

##### Rights and Obligations

This section is the core of any OSH-license
since it governs what the licensee can and cannot do with the licensed OSH.

Usually, OSH-license include the grant of a perpetual,
worldwide, non-exclusive, no-charge, royalty-free, irrevocable license
to specific rights such as copyright or patent.
This means that the license is unlimited in time (perpetual)
and works everywhere in the world (worldwide).
Furthermore, you do not have to pay for it (no-charge; royalty-free)
and the license cannot be taken back by the licensor (irrevocable).

In exchange, if you republish the licensed material or part of it
or if you publish any modifications thereof,
OSH-licenses usually require you to give notice.
In particular, this can include the following obligations:

- You must give recipients of the licensed Project or your modifications thereof
  a copy of the license

- You must indicate the parts of a Project you modified or changed

- In case of modifications, you must indicate where you found the source material

- You must retain any legal notice previously in place
  and pass it on to the next recipient

- You must retain any other notice and pass it on to the next recipient

In addition, and this is crucial,
most OSH-licenses will require you to make any modifications you publish
available under the same license.
For instance, if you downloaded and modified design
in a CAD-file licensed under CC-BY-SA 4.0,
the license requires you to use the same CC-BY-SA 4.0 license,
a later version of it or a license that is compatible with the CC-BY-SA 4.0,
i.e. that has the same standards.
This often called the “viral effect” of Open Source Licenses <!-- TODO add "is" -->
and works especially well with copyright,
because as mentioned above, copyright arises automatically.
For addressing difficulties when dealing with registered rights such as patents,
see below.

##### Disclaimer and Liability

This section limits the licensor’s liability as far as permitted by law.

The current set of OSH licenses takes into account,
that for most developers, OSH consists of projects they are passionate about.
However, they usually do not pursue these project for financial gain
and mostly in their spare time.
This is the reason, why OSH developers usually do not have an interest
in taking any legal responsibility for the quality of the licensed OSH.
In addition, as OSH is usually available free of charge,
licensors usually try to avoid any legal liability for damages.

The reason that liability is only excluded as far as permitted by law
is that in this regard, the law can be very different from country to country.
US-law for instance is a typical example for a very strict legal regime,
where high punitive damages are a real risk.
In addition, in most countries,
liability for certain damages such as damages to people cannot legally be excluded
(so-called “strict liability”).

For more on OSH and liability, see below under “Liability” (To-Do).

##### Termination

This section in OSH licenses stipulates, under which conditions a license will terminate.

As mentioned before, OSH licenses are usually not limited in time.
However, especially in case of IPleft Licenses,
if you do not comply with the terms of a license,
it will automatically terminate.
This means that, for example,
if you do not attach a copy of the license for subsequent users
while publishing your modifications to a design,
your own license will terminate,
and you will automatically be subject to design or copyright infringement.

This means
that special care should be taken to fulfill the requirements of any OHS license.
For examples on how to do this, see below.

#### OSH licensing and Patents

#### Compatibility of OSH-licenses

<!-- Note:
Explain viral effect of copyright licenses.
Explain difficulty without exclusive rights (technical products without patent).
Explain defensive publishing -->

1. Starting point: Exclusive Rights
2. The viral effect of Open Licenses (general notes)
3. Infringement of licensing conditions
    1. Compatibility with OSH project
    2. Recommendations

### Overview over selected licenses and how they work

#### Open Source Hardware Licenses

Open Hardware Licenses have mostly been inspired by Copyleft Licenses for software
and then adapted to the specific cases of hardware
([@katzFunctionalLicenseOpen2012], pp. 41-42)

##### CERN OHL

CERN OHL is a popular Open Hardware License
developed by the European Organization for Nuclear Research.
The license refers to the documentation of the design of hardware.
It regulates its use, copying, modification and distribution,
as well as the manufacture and distribution of the product [@CERNLaunchesOpen].
The documentation can have the form of diagrams,
circuit board layouts, descriptive text, etc.
The latest version of the license can be obtained from the Open Hardware Repository,
together with a guideline [@CernOhlVersion].
Both must be included if the documentation is copied or modified and distributed.

CERN OHL can be used for defining a patented hardware design as open source
("non-exclusive license to a patent held by the Licensor").
A Licensor (who publishes the design documentation) can express the wish
to be informed about any production of the hardware,
but compliance by the Licensee (who copies the design documentation)
in this case is voluntary.

A disclaimer of warranty stipulates that all risk associated with
the documentation (e.g. infringement of patents of third parties)
and with the product (e.g. fitness for a particular purpose)
is with the Licensee and not the Licensor.
Accordingly, the Licensor shall not be held liable for any possible damages
occurring in conjunction with the documentation or product.
If the Licensee violates the CERN OHL or takes legal action against the Licensor,
s/he cannot benefit from this open license of the documentation anymore.

Note: Submodules can be licensed differently.

###### CERN OHL 2.0

- S
- W
- P

The CERN OHL 2.0 is the most recent version of the CERN OHL. In contrast to the CERN 1.2, it does not consist of a single license, but three different licenses. These are targeted to adress different appproaches to use of OSH. Now, there is a permissive license (p), a weak ipleft license (w) and a strong ipleft license (s).

With regard to the interpretation of these licenses, CERN published a rationale document and a how-to-guide to each license. Until there are any court decisions, these documents should prove useful for questions regarding the use of the licenses.

###### General remarks regarding all CERN OHL 2.0. licenses

- available components
- rationale behind distinction between w and s
- licenses are tailored to adress osh circuit boards
- licenses probably do not cover sales of OSH products
- combination of CERN 2.0 licenses
- private distribution vs publication
- compatibility to GPL 3.0

###### The CERN OHL 2.0 permissive license (p)

###### The CERN OHL 2.0 weak ipleft license (w)

###### The CERN OHL 2.0 strong ipleft license (s)


##### TAPR OHL

The Tucson Amateur Packet Radio Open Hardware License was the first of its kind,
and drafted by the lawyer John Ackermann in 2007
([@ackermanOpenSourceHardware2008a], p. 204).
The license text can be obtained from the tapr.org website [@TAPROpenHardware].
In addition to being a license, it is also a contract,
stipulating that the documentation must not only be passed on downstream,
but also upstream to earlier developers as far as a contact was provided
([@ackermanOpenSourceHardware2008a], pp. 208-209).
The license allows for the documentation to be copied, modified,
and distributed together with the TAPR OHL.
The "before" and "after" versions of the files need to be included.
Regulations regarding patents, warranty and liability
are similar to the later CERN OHL (see above).

**[TODO]**

c. (DRAFT) Apache 2.0 and Solderpad 2.1

The Apache 2.0 and Solderpad 2.1 licenses are two permissive licenses,
where essentially Apache 2.0 is a permissive software license
and Solderpad 2.1 acts as a wraparound for associated hardware projects.

Apache is a Software Foundation founded in 1999 that develops,
supports and incubates software projects in categories such as Artificial Intelligence,
Bigs Data, Cloud Computing etc..
According to its mission statement,
one of their missions is to provide Open Source software for the public good^[apache-org].
To this end, Apache created the Apache Open Source Software license^[apache2.0].
The Solderpad license^[shl2.1] was later created
by the Free and Open Source Silicon Foundation (FOSSi),
a non-profit organization with the mission to promote and assist
free and open digital hardware designs and their related ecosystems^[fossi-found].

[apache-org]:
see <https://www.apache.org/>

[apache2.0]:
available at <https://www.apache.org/licenses/LICENSE-2.0>

[shl2.1]:
available at <http://solderpad.org/licenses/SHL-2.1/>

[fossi-found]:
see <https://www.fossi-foundation.org/licensing>

The current version of the Apache license, Apache 2.0,
covers Copyright and Patent rights to the licensed material.
While it can primarily be seen as a Software license,
its scope is not limited to source or object code.
Rather, according to its definitions,
“source form” is the preferred form for making modifications,
while “object form” is the form resulting from mechanical transformation
or translation of source form.
Being a permissive license,
the Apache License does not require you to make your modifications available
under the same conditions as the licensed material.
However, if you send your modifications intentionally back to the licensor
for inclusion into the licensed material,
the licensing conditions will also apply to your modifications,
unless you expressly object or have a separate agreement with the licensor
(see Apache 2.0, no. 5).

Otherwise, Apache 2.0 essentially requires you (Apache 2.0, no.4) to:

- give any recipient of the licensed material or any modification
  a copy of Apache 2.0;
- give notice that you modified the licensed material if you did;
- retain any copyright, patent, trademark or attribution notices

In addition, in case Patents are part of the licensed material,
you must not start a patent invalidity action,
because otherwise the license automatically terminates.

Solderpad 2.1, as a wraparound, is a very short license
that simply adds to, or changes the wording of, Apache 2.0
(see Solderpad 2.1, no. 1).
For example, Solderpad 2.1 clarifies that “source form” also means “board layouts,
CAD files, documentation source, and configuration files”.
In addition, “object form” also means the application of a Source form
to physical material and specifically includes
“instantiation of a hardware design or physical object or material
and conversions to other media types,
including intermediate forms such as bytecodes, FPGA bitstreams, moulds,
artwork and semiconductor topographies (mask works)”.
In contrast to Apache 2.0, Solderpad 2.1 covers Copyright, Design rights,
rights in semiconductor topographies and database rights,
excluding patents and trademarks.

Considering Apache was originally designed for Software projects,
it is probably not a good fit for OSH projects.
One reason is that its definitions,
for instance the division between source form and object form,
a clearly tailored for Software.
In case of Hardware, this division is not necessary
and may lead to confusion in the interpretation of the license.
For instance, in case of hardware, the license does not clarify, what “object form”,
meaning “any form resulting from mechanical transformation
or translation of a Source form” would look like.

Solderpad 2.1 on the other hand
might be a good approach regarding projects involving semiconductors
or reprogrammable logic chips (FPGAs),
since the license was originally developed to address this technology^[fossi-found].
One downside in using Solderpad 2.1 for other projects,
however, is that it probably does not cover Patent rights.
In this regard, the exclusion of Patent rights
in the rights’ definition in Solderpad 2.1 seems confusing.
On the one hand, Apache 2.0 already contains a license grant for patents
and according to Solderpad 2.1, no. 1,
the definition for “Rights” in Solderpad is an “new definition”.
This could mean that the original Patent license in Apache 2.0 stays in effect.
On the other hand,
the rights’ definition in Solderpad 2.1 specifically mentions Copyright,
which would not have been necessary
when the original Copyright license of Apache 2.0. was left in place.
In addition, regarding the “license grant” in Solderpad 2.1, no. 2,
the license does not clarify whether this license grant replaces the original Copyright
and Patent licenses in Apache 2.0.

In summary, Apache 2.0 and Solderpad 2.1 are permissive licenses
that probably work best
applied to Software or semiconductor and FPGA-related OSH projects.
The benefit of Solderpad 2.1 is,
that it additionally grants a license in Design rights,
rights to semiconductors and database rights.
With regard to Patent rights, however,
it seems likely that these rights are not included in Solderpad 2.1.
From a licensor point of view,
the lack of clarity regarding Patent rights should not be problematic,
since Solderpad 2.1 is a permissive license.
Using this license,
a licensor should generally have little interest
in restricting use of the licensed material.
Lack of clarity could, however,
deter licensees from using material licensed under Solderpad 2.1.
This is because a licensee will have to rely on the license’s grant or rights
and might feel at risk for infringement,
if the scope of the license is not sufficiently clear.

d. CC

e. CC by

f. CC by sa

#### Free/Open Source Software Licenses

**[TODO]**

- Apache
- GPL
  - See [this statement from the OHWR](
    https://ohwr.org/project/cernohl/wikis/faq#q-why-not-use-existing-licences-such-as-gpl-and-any-in-the-family-of-creative-commons-licences)
    why GPL is not an optimal choice for OSH.
- LGPL
- MIT
- Mozilla

#### Dual- and Multi-Licensing

**[TODO]**

#### Patenting your invention and associated protective mechanisms

**[TODO]**

1. What are the benefits of patenting my OSH-project?
2. What are the downsides of patenting?
3. Open Source mechanisms in case of patenting
    1. Patent Pledges
    2. Patent Peace Provisions
    3. Defensive Patent License

#### Re-licensing Hardware

**[TODO]**

<!-- Umlizenzierung von "falscher" Lizenz auf "richtige" Hardwarelizenz; grober Überblick, keine Details von Lizenz zu Lizenz -->

## Keeping your Freedom to Operate

How to avoid lock-out and Patent Infringement suits.

### Introduction: Keeping your Freedom to Operate

<!-- Note: Explain problems around Patent Trolls being locked out of our own invention or next steps in the inventive process, risk of (un)lawful lawsuit -->

Open Source Hardware processes seem to be vulnerable
to being stopped in their tracks by the patent system.

The reasons for this are probably twofold.
First, due to limited resources,
OSH projects may often not be able to obtain a patent on their invention.
Not being able to patent your invention, however,
makes you vulnerable to being locked out of using the invention
when someone else patents it before you do
(unless you publish the invention in a sufficient manner;
see below under "Defensive Publishing).

Secondly, there is the so-called "patent thicket".
The term "patent thicket" quite adequately describes the system
of legally enforceable patents worldwide
and its relationship in particular with complex products.
Of all the registered patents currently in force,
about half are not of any practical use.[^66]
This half seems to be used to block competitors from entering the market.[^67]
Since patents often overlap in their scope of application,
it is usually very difficult and takes a lot of time and energy
to figure out whether an invention is already patented or not
(cf. [@shapiroNavigatingPatentThicket2001]).

The ones frequently benefiting from these mechanisms
and therefore one of the greatest threats to OSH
are so-called Patent Trolls.
Patent Trolls are usually Non-Practicing-Entities (NPEs).
These entities own patents which they not effectively use
to produce the patented invention.
They rather use their patents to generate profit
by suing others who allegedly infringe their patents.[^69]

[^69]: needs citation

The following chapter will therefore give an overview
of how best to avoid the Lock-out
and minimize the risk of unlawful patent infringement
as well as lawful patent infringement.

### Avoiding the Lock-out Problem

_In short, how to avoid downstream patenting of (parts of) the invention:_\
exploit novelty/inventive step requirement by defensive publishing
of OSH with notice to Patent Offices.

#### How you can get locked-out

Being locked-out of your own invention generally means
that someone else patents your invention
and obtains the exclusive rights to it.
Other cases possibly include someone else registering a trademark
or obtaining other IP rights like designs in relation to your invention.
Consequently, use of your own invention may be prohibited or restricted.

This problem seems to be most relevant for next development steps in an OSH-project.
Exactly because OSH is open source and the development process of new hardware
is usually documented and published,
it might be easy for a third party to figure out the next logical step
in the development process and patent it,
which obviously hinders the further development for the community.
However, everything that _has_ been published already under a free/open license
_theoretically_ cannot be patented anymore anywhere.

#### Strategies to avoid Lock-out

##### Defensive Publishing

###### General description

Defensive Publishing means that you publish your invention in a way
that it becomes State of the Art and thus destroy its novelty
(examples may be found on prior art databases like [@TechnicalDisclosureCommons]).
An example would probably be the publication of your invention
in a known professional journal,
but inventions may also be self-published to become state of the art
(cf. [@DefensivePublicationsCostEffective2020]).

If an invention becomes state of the art, it can generally not be patented by anyone.

###### Requirements to Publication

The requirements for publication are usually not very high
with regard to defensive publishing.

Patent-like publication for instance is not required.
The term State of the Art does not refer to the format,
but rather the information itself.
The technology behind a quill, for example, is clearly State of the Art,
although it is not available in patent-like form.

Another aspect to consider is,
that your target group for defensive publishing needs not to be very big.
You can even publish your invention nationally
for it to become part of the _global_ state of the art.
However, it is crucial in order for defensive publishing to work,
that the patent office or the courts take notice of it.
This means that you must be able to produce evidence on the publication of your invention.

As a general rule of thumb, while the requirements for publication are a grey area,
it seems to be relatively easy to damage patentability,
since disclosure usually refers more to making knowledge available
than to uphold certain formalities.

###### Limitations of Defensive Publishing

Defensive Publishing has some important limitations
to be considered with regard to the problem of lock-out.

Firstly, while the requirements for Defensive Publishing might not be high,
there did not use to be an adequate standard for the documentation
of OSH or similar projects.
This does not only make it difficult for others to use the available OSH,
but also it makes it difficult for the IPOs to assess disclosure and prior art.
That means there is the risk that your publication might go unnoticed
or is deemed to be not sufficient to count as prior art.

Secondly, defensive publishing does only work
to prevent registration of IP Rights which require novelty.
The registrations of Trademarks, for example,
is usually not subject to novelty of the corresponding sign.
Consequently, it seems that Defensive Publishing might not prevent others
from registering a Trademark for goods or services associated with your project.

###### Best practices/ Do's and Don'ts

- use resilient, original timestamps. e.g. by using git
- bring publication to the attention of Intellectual Property Offices (IPOs),
  e.g. by using well-known platforms
- Usually, the better the Documentation,
  the better the chances are to count as sufficient disclosure.
- If available, use of a certain standard like DIN SPEC 3105-1[^DINSPEC]
  might improve the chances of success for Defensive Publishing.

[^DINSPEC]: available at <https://www.din.de/de/wdc-beuth:din21:324805763> and [@10OSEGermanyOpen].

#### Streamlining openness in product development processes

Defensive publishing can prove to be a good defence against Lock-out,
but it does not solve the problem of somebody else anticipating
the logical next steps of your invention.

For working around this problem, there may be several solutions:

##### Strong Open Source Product Development

Adopting the strategy of Strong Open Source Product Development,
essentially means to rely on Defensive Publishing as protection mechanism
and publish as often and as much as possible.

Since today, there seems to be a huge background of state of the art,
it is likely that inventions must be very specific to be patentable at all.
Therefore, particularly in cases concerning smaller OSH Projects,
the chances of being patented by a third party
might not be very high in most OSH cases.

Therefore, in general terms, strong OSH development by itself
might already be enough to avoid Lock-out.

##### Safe Open Source Product Development

Another possible strategy could be
to treat the development of your OSH as a trade secret for a limited time,
e.g. until development is finished or until certain milestones have been reached.
Then you could prepare a well-documented public release in terms of Defensive Publishing
to destroy novelty with regard to your Project.

This strategy probably has the benefit,
that at least a certain version of your Project will become state of the art
and consequently not be patentable any more.
On the other hand, this may only apply to the then published version of your Project.
Any future versions may still be subject to the patenting of third parties,
if they keep enough distance to the original
or if they have completely new features not present in the original design.

It must further be noted, that this strategy probably requires you
to implement security measures like Non-Disclosure Agreements (NDAs),
to keep the Project secret in order to be protectable as a trade secret.
And that's of course in contrast to how open source communities usually work.

##### Use of Copyleft effects for OSH with own patents

In more specific cases, registering your own patent might be even more helpful.

If you are able to patent your invention yourself,
you possibly avoid future Lock-out
due to the fact that you are in control of the exclusive patent rights.
Anyone trying to develop the next step to your patented invention
will probably have to use your patented technology in the process.
Without your permission, this probably constitutes infringement of your patent.

There are, however, some disadvantages to this solution.
Firstly, this could make the development process less open,
since you would have to keep the development secret until patenting
to not destroy patentability.
Secondly, effectively protecting your invention
requires you to monitor use of your patent
and to enforce it against others, which requires time and money.

If you do not have a patent, you will probably need to use another workaround,
since currently, there does not seem to be any enforceable copyleft meachism available
for functional parts without a patent (or utility patent).

### Minimizing risk of getting sued for patent infringement

Besides the risk of someone else patenting the next steps of your OSH,
another substantial risk to freedom to operate
is usually the threat of litigation for patent or other IP infringement.

If IP litigation is successful,
usually the infringer is ordered to stop using the Litigation
for patent or other IP infringement
is usually associated with high costs[^74] and takes up a lot of time,
during which use of the contested OSH may not be permitted.[^75]
Therefore, the mere threat of litigation may be enough,
particularly for individual developers,
to discourage development, distribution and use of OSH.

[^74]: citation needed
[^75]: citation needed

#### Wrongful patent enforcement

In case of wrongful patent enforcement, e.g. by patent trolls,
there is theoretically no risk
that you might be stopped from using the contested OSH,
since the infringement suit is unlawful.
However, this might not be clear from the outset
and since patent litigation might be costly and time-consuming,
unlawful patent enforcement in practice might pose a real problem to OSH developers.

Unfortunately, there does not seem to be a clear remedy
for wrongful patent enforcement at the moment.
For example, globally speaking, there does not seem to be a use-requirement for patents.
This means that even entities that do not use their patents
may sue OSH developers for patent infringement.

#### Lawful patent enforcement

In case of lawful patent enforcement,
i.e. if use of OSH does indeed infringe patent rights,
all use of the relevant patent -
and therefore the infringing OSH -
will probably be stopped.
However, there are some limitations to patent law
and some strategies to be considered
to reduce the risk of lawful patent enforcement against OSH projects.

##### Territorial scope of patent rights

One important aspect to be considered,
is that patent rights generally only take effect
in the countries where a patent is registered.
This means that if the use of an invention is stopped in one country,
this does generally not mean
that the use in other countries is automatically forbidden as well.

##### Attribution of inventorship and advertisement as "for free"

Another possible strategy to reduce the risk for patent infringement in OSH-cases
might be to refrain from claiming inventorship of a certain invention.

As sole publication of a patented invention might generally not trigger patent infringement
(the patent is already published by the patent owner),
you might not be liable for infringement
if you do not claim to be the creator of the patented invention.

The same applies to advertising an invention as "for free".
If the invention is patented, this would probably trigger patent infringement,
as the patent owner usually has the right to offer their invention for free
or in exchange for license fees, etc.

However, if you are passing on instructions on how to build a patented Product,
or even kits to assemble it,
the case might be different.
There is a chance that these acts might be deemed lending aid to patent infringement.
This is because offering instructions - or even kits -
might be more than just passing on the details of the patented invention.

##### Empty Pockets - making yourself unattractive for patent litigation

Another way to avoid patent litigation
is to make yourself financially unattractive
and thus in practice limiting the amount of money you would be able to pay
in case of an infringement claim.
The downside of this strategy might be,
that in case of a patent infringement suit,
you may not be able to defend yourself and might need to stop your operations completely.

#### The private use exception and its limitations

Another possible way to escape patent litigation might be
to rely on a private use exception in national patent laws
(PatG, § 11 Nr. 1 [@11PatGEinzelnorm]).

The scope of this exception is, however, very narrow.
Generally, only use in the private sphere -
possibly only between individual persons as opposed to legal persons -
and only for non-commercial purposes might be allowed.
This means that with regard to OSH projects,
the private use exception might only be an option in the early stages of a Project
(e.g. "tinkering in the garage")
and before a project is rolled out to a greater public.
Otherwise, there is the risk that relying on the private use exception
might be considered fraudulent.

### Summary

In summary, with the current state of patent law,
it might be very difficult for successful OSH Projects
to escape unlawful or lawful patent litigation.
On the other hand, there are some measures
(in particular Defensive Publishing, streamling the OSH development process),
which might prove useful to reduce the risk of patent infringement.

## Liability risks in OSH

**[TODO]**

…and how to minimize them

- short introduction
- What happens if Product based on published Documentation injures people?
  Who is liable?
- Contractual liability
- Avoiding liability through OS licenses*
- Tort liability
- Liability for intentional harm and negligence.
  Do's and don'ts?
- What is the standard for negligent behavior in case of OSH?
- Product liability
- Note: Applicability of strict liability to OSH-developers.
  Is strict liability avoidable?

## Do's and Don'ts

**[TODO]**

# Glossary

**[TODO]**

- Check References to DIN Spec 3105-1
- IP Right or IP Rights
- OSH
- FOSS
- OSH Project
- Hardware
- Software
- documentation
- Branding
- Use
- Application
- Distribution Channels

# Annex

- Links to OSH-compatible licenses
- Caselaw-index
- Literature/videos/useful sources/slides, etc
- Points of contact/FAQs/events/organizations/law clinics

# Bibliography
