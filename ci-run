#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2021 Robin Vobruba <hoijui.quaero@gmail.com>
#
# SPDX-License-Identifier: Unlicense

# Exit immediately on each error and unset variable;
# see: https://vaneyckt.io/posts/safer_bash_scripts_with_set_euxo_pipefail/
set -Eeuo pipefail
#set -Eeu

script_dir=$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")
#sudo=$(which sudo && echo "sudo" || echo "")

MVD_CLONE_URL="https://github.com/movedo/MoVeDo.git"
MVD_HOME_DEFAULT="$HOME/.local/opt/movedo"
# Whether to install movedo (in MVD_HOME_DEFAULT) without asking,
# if not found.
INSTALL_YES=false

function print_help() {

	echo "Run all we want to be done in our CI."
	echo
	echo "Usage:"
	echo "         $(basename "$0") [OPTIONS] <rdf-file> [rdf-file...]"
	echo "Options:"
	echo "          -h, --help                  Show this help message"
	echo "          -c, --check-style           Check Markdown style in the whole project"
	echo "          --force-python3             WARNING! Sets python3 as the python executable, systen wide!"
	echo "          -s, --install-requirements  Install whatever this script needs to run (works only on Debian/Ubuntu)"
}

function check_style() {

	echo -n "mdl version: "
	mdl --version

	# Check Markdown style (excluding LICENSE.md)
	find . -maxdepth 1 -type f -name "*.md" ! -name LICENSE.md -print0 \
		| xargs -0 mdl --git-recurse --ignore-front-matter --config .mdlrc
}

function unset_if_empty() {

	var_name="$1"
	if [ -z "${!var_name}" ]
	then
		unset "$var_name"
	fi
}

function initializing_vars() {

	remote_http_url="$(git ls-remote --get-url origin | sed -e 's|:|/|' -e 's|^git@|https://|' -e 's|\.git$||')"

	set -a
	CI_COMMIT_BRANCH="${CI_COMMIT_BRANCH:-$(git branch --show-current)}"
	unset_if_empty CI_COMMIT_BRANCH
	CI_COMMIT_TAG="${CI_COMMIT_TAG:-$(git tag --points-at HEAD)}"
	unset_if_empty CI_COMMIT_TAG
	CI_COMMIT_SHA="${CI_COMMIT_SHA:-$(git rev-parse --short HEAD)}"
	unset_if_empty CI_COMMIT_SHA
	MVD_HOME="${MVD_HOME:-$MVD_HOME_DEFAULT}"
	PATH="$MVD_HOME/bin:$PATH"
	PUBLISH_DIR="${PUBLISH_DIR:-$(pwd)/public}"
	REF="${REF:-$(git describe --long --dirty --candidates=99 --always --first-parent)}"
	PUBLISH_DIR_TMP="${PUBLISH_DIR}_TMP"
	PROJECT_NAME="${CI_PROJECT_NAME:-$(cd "$script_dir/"; basename "$(pwd)")}"
	CI_PROJECT_URL="${CI_PROJECT_URL:-$remote_http_url}"
	set +a
}

function install_movedo() {
	if [ -e "$MVD_HOME" ]
	then
		(
			cd "$MVD_HOME"
			git fetch
			git rebase origin/master
		)
	else
		mkdir -p "$(dirname "$MVD_HOME_PARENT")"
		git clone "$MVD_CLONE_URL" "$MVD_HOME"
	fi
}

function ensure_movedo() {

	if ! which mvd > /dev/null
	then
		>&2 echo "WARNING: MoVeDo (executable 'mvd') not found in PATH"
		if $INSTALL_YES
		then
			install_movedo
		else
			echo "Would you like to install MoVeDo now?"
			echo "It involves only a 'git clone' of its repo"
			echo "and setting two env-vars."
			echo "Install location (MVD_HOME): '$MVD_HOME'"
			echo
			while true
			do
				read -r -p "Install MoVeDo?" yn
				case $yn in
					[Yy]*)
						install_movedo
						break
						;;
					[Nn]*)
						>&2 echo "ERROR: MoVeDo (executable 'mvd') is required but was not found."
						exit
						;;
					*)
						echo "Please answer yes or no."
						;;
				esac
			done
		fi
	fi
}

function install_requirements() {

	initializing_vars
	ensure_movedo
	mvd ensure_propper_repo
	mvd install_pdsite
}

function force_use_python_3_system_wide() {
	# Make Python 3 the default, so pandoc will use it to run panflute,
	# which requires at least Python 3.6
	rm -f /usr/bin/python
	ln -s /usr/bin/python3 /usr/bin/python
}

# read command-line args
POSITIONAL=()
while [[ $# -gt 0 ]]
do
	arg="$1"
	shift # past argument

	case "${arg}" in
		-h|--help)
			print_help
			exit 0
			;;
		-c|--check-style)
			check_style
			exit 0
			;;
		--force-python3)
			force_use_python_3_system_wide
			exit 0
			;;
		-s|--install-requirements)
			install_requirements
			exit 0
			;;
		-y|--yes)
			INSTALL_YES=true
			;;
		*) # non-/unknown option
			POSITIONAL+=("${arg}") # save it in an array for later
			;;
	esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

initializing_vars

echo "Publishing to '$PUBLISH_DIR' ..."

# Load the PATH additions from the MoVeDo `setup` script
. "$HOME/.bashrc"
. "$HOME/.profile"

python --version

which mvd
echo "MVD_HOME: '$MVD_HOME'"
if ! [ -d "$MVD_HOME/scripts" ]
then
	>&2 echo "ERROR: Invalid value for MVD_HOME!"
fi

# Check if `pandoc` can be found
which pandoc
# ... and log the version info
mvd print_pandoc_version

# Check if `panflute` can be found
which panflute
# ... and log the version info
mvd print_panflute_version

# Remove content generated in previous builds
mvd clean

# * Generate sources (plain Markdown from PP-Markdown & co.)
# * Build our PDF output
# * Build our HTML site
export MVD_DEBUG=true
export MVD_SINGLE_FILE=false
export MVD_INDIVIDUAL_PDFS=true
export MVD_REPO_URL="${CI_PROJECT_URL:-$script_dir}"
mvd build

# cleanup the output
rm -f build/pdf/doc.pdf

# move output to the location that will be pushed to GitLab-pages
# We remove this here to make sure that all old build artifacts are gone.
rm -Rf "$PUBLISH_DIR_TMP"
mkdir -p "$PUBLISH_DIR_TMP"
cp -r build/html/* "$PUBLISH_DIR_TMP/"
cp -r build/pdf/* "$PUBLISH_DIR_TMP/"
find build/gen_sources/ -name "*.md"  -exec cp -t "$PUBLISH_DIR_TMP" {} +
cp build/gen_sources/VERSION "$PUBLISH_DIR_TMP"

# Create an index.html linking to all the generated files
# if the project does not provide an index.md|html its self,
# or by means of its static-site generator (SSG).
if ! [ -e "$PUBLISH_DIR_TMP/index.html" ]
then
	pushd "$PUBLISH_DIR_TMP"
		mvd create_index "$PUBLISH_DIR_TMP" "index.html_TMP"
		mv index.html_TMP index.html
	popd
fi

# package the output
mkdir -p build/package
git clone . build/package/src
cp -r build/html/* build/package/
cp -r build/pdf/* build/package/
cp "$PUBLISH_DIR_TMP/"*.md build/package/
cp build/gen_sources/VERSION build/package/
pushd build/package
	rm -f index-md.txt
	rmdir --ignore-fail-on-non-empty illustrations
	rm -f ./*.tex
	zip -r "$PUBLISH_DIR_TMP/package.zip" ./*
popd
pushd "$PUBLISH_DIR_TMP"
	ln "package.zip" "${PROJECT_NAME}-${REF}.zip"
popd

# We replace the publishing dir at the very end only,
# so as not to remove old build artifacts in case of a failed build.
rm -Rf "$PUBLISH_DIR"
mv "$PUBLISH_DIR_TMP" "$PUBLISH_DIR"

# List all the published files
echo
echo "# All published files:"
find "$PUBLISH_DIR"

